/**********************************************************************
* � 2005 Microchip Technology Inc.
*
* FileName:        adcDrv1.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       dsPIC33Fxxxx
* Compiler:        MPLAB� C30 v3.00 or higher
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author            Date      Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Settu D 			03/09/06  First release of source file
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* ADDITIONAL NOTES:
* This file contains two functions - initAdc1(), initTmr3(), _initDma0()
* and _DMA0Interrupt functions
**********************************************************************/

#if defined(__dsPIC33F__)
#include "p33fxxxx.h"
#elif defined(__PIC24F__)
#include "p24Fxxxx.h"
#endif

#include "adcDrv1.h"
#include "dsp.h"
#include "tglPin.h"
#include "display.h"

//extern IIRTransposedStruct ExampleHPFFilter;
//extern void IIRTransposedInit ( IIRTransposedStruct* filter );
//extern fractional* IIRTransposed (      int numSamps,
//                                        fractional* dstSamps,
//                                        fractional* srcSamps,
//                                        IIRTransposedStruct* filter
//                                );







/*=============================================================================
initAdc1() is used to configure A/D to convert channel 5 on Timer event. 
It generates event to DMA on every sample/convert sequence. ADC clock is configured at 625Khz 
=============================================================================*/
void initAdc1(void)
{

		AD1CON1bits.FORM   = 0;		// Data Output Format: Unsigned Int
		//AD1CON1bits.SSRC   = 2;		// Sample Clock Source: Timer 3 starts conversion
		AD1CON1bits.SSRC   = 0;		// Sample Clock Source: SAMP starts conversion
		AD1CON1bits.ASAM   = 1;		// ADC Sample Control: Sampling begins immediately after conversion

		AD1CON2bits.SMPI    = 15;	// Number of conversions before interrupt
        AD1CON2bits.VCFG    = 4;    // Refs are Vdd and Vcc
        AD1CON2bits.CSCNA   = 0;    //Do not scan
        AD1CON2bits.ALTS    = 0;    //Always use MUXA
        

        AD1CON3bits.ADRC    = 0;	// ADC Clock is derived from Systems Clock
		AD1CON3bits.ADCS    = 63;	// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*64 = 1.6us (625Khz)
									// ADC Conversion Time for 12-bit Tc=14*Tad = 22.4us 

        
        //AD1CHS0: A/D Input Select Register
        AD1CHSbits.CH0SA1=0;		// MUXA +ve input selection (AN0) for CH0
		AD1CHSbits.CH0NA=0;         // MUXA -ve input selection (Vref-) for CH0

        //AD1PCFGH/AD1PCFGL: Port Configuration Register
		AD1PCFG = 0xFFFF;             // set all to digital band gap ref is enabled
        AD1PCFGbits.PCFG1 = 0;		// AN0 as Analog Input

        AD1CHS = 0x0005;// Positive sample input channel for MUX A to use AN5,Negative input channel for MUX A to use VR-
        AD1PCFG = 0xFFDF; //AN5 analog pin
     	AD1CSSL	= 0x0000; // Channel Scanning Disabled

        
        IFS0bits.AD1IF = 0;			// Clear the A/D interrupt flag bit
        IEC0bits.AD1IE = 0;			// Disable A/D interrupt 
        AD1CON1bits.ADON = 1;		// Turn on the A/D converter	

        tglPinInit();

}

/*=============================================================================  
Timer 3 is setup to time-out every 125 microseconds (8Khz Rate). As a result, the module 
will stop sampling and trigger a conversion on every Timer3 time-out, i.e., Ts=125us. 
At that time, the conversion process starts and completes Tc=14*Tad periods later.

When the conversion completes, the module starts sampling again. However, since Timer3 
is already on and counting, about (Ts-Tc)us later, Timer3 will expire again and trigger 
next conversion. 
=============================================================================*/
void initTmr3() 
{
        TMR3 = 0x0000;
        PR3 = SAMPPRD;
        IFS0bits.T3IF = 0;
        IEC0bits.T3IE = 0;
    
        //Start Timer 3
        T3CONbits.TON = 1;

}






//void __attribute__((interrupt, no_auto_psv)) _ADC1Interrupt(void)
//{
//
//	displayByte( (uint8_t) BufferA[0] );
//
//	tglPin();               // Toggle RA6	
//    IFS0bits.AD1IF = 0;		//Clear the ADC Interrupt Flag
//}



int getADCAvg(void)
{
    unsigned long int temp; int avg;

    temp = 	ADC1BUF0 + ADC1BUF1 + ADC1BUF2 + ADC1BUF3 + 
            ADC1BUF4 + ADC1BUF5 + ADC1BUF6 + ADC1BUF7 + 
            ADC1BUF8 + ADC1BUF9 + ADC1BUFA + ADC1BUFB + 
            ADC1BUFC + ADC1BUFD + ADC1BUFE + ADC1BUFF;
    avg = temp/16;     // 2048 = 8/(16*1024)  take average, scale 10-bits to 3-bits.
    return (avg);
}




