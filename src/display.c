#include "display.h"
#include <xc.h>
uint8_t bitRead(uint8_t numToShow, uint8_t index) {
    return ((numToShow  & ( 1 << index))>> index);
}

void displayMinutes(uint8_t numToShow)
{
    PORTBbits.RB9 =  bitRead(numToShow, 3);
    PORTBbits.RB8 =  bitRead(numToShow, 2);
    PORTBbits.RB7 =  bitRead(numToShow, 1);
    PORTBbits.RB6 =  bitRead(numToShow, 0);
    
}
void displayHours(uint8_t numToShow)
{
    PORTBbits.RB13 =  bitRead(numToShow, 3);
    PORTBbits.RB12 =  bitRead(numToShow, 2);
    PORTBbits.RB11 =  bitRead(numToShow, 1);
    PORTBbits.RB10 =  bitRead(numToShow, 0);
}
void displayByte(uint8_t numToShow)
{
    PORTBbits.RB13 =  bitRead(numToShow, 0);
    PORTBbits.RB12 =  bitRead(numToShow, 1);
    PORTBbits.RB11 =  bitRead(numToShow, 2);
    PORTBbits.RB10 =  bitRead(numToShow, 3);
    PORTBbits.RB9 =  bitRead(numToShow, 4);
    PORTBbits.RB8 =  bitRead(numToShow, 5);
    PORTBbits.RB7 =  bitRead(numToShow, 6);
    PORTBbits.RB6 =  bitRead(numToShow, 7);

}