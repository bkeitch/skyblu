/* 
 * File:   display.h
 * Author: BENITC
 *
 * Created on 23 May 2017, 11:28
 */

#ifndef DISPLAY_H
#define	DISPLAY_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

uint8_t bitRead(uint8_t numToShow, uint8_t index) ;

void displayMinutes(uint8_t numToShow);
void displayHours(uint8_t numToShow);
void displayByte(uint8_t numToShow);

#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAY_H */

