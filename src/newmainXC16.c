#include <stdint.h>
#if defined(__PIC24F__)
#include "p24fxxxx.h"
#endif
#include <xc.h>
#include "adcDrv1.h"
#include "dsp.h"
#include "display.h"
#include "uart2.h"
#include <stdio.h>
#include <libpic30.h> 

unsigned int BufferA[NUMSAMP] ;

//Macros for Configuration Fuse Registers:
//The fuses select the oscillator source, power-up timers, watch-dog
//timers etc. The macros are defined within the device
//header files. The configuration fuse registers reside in Flash memory.


//set the configuration bits: internal OSC, everything off except MCLR

// Internal FRC Oscillator
//_CONFIG2(FNOSC_FRCPLL &  FCKSM_CSECMD & OSCIOFNC_OFF  & POSCMOD_NONE & FWDTEN_OFF)
// FRC Oscillator
// Clock Switching is enabled and Fail Safe Clock Monitor is disabled
// OSC2 Pin Function: OSC2 is Clock Output
// Primary Oscillator Mode: Disabled


// Watchdog Timer Enabled/disabled by user software

_CONFIG1(JTAGEN_OFF & GCP_OFF & GWRP_OFF & ICS_PGx1 & FWDTEN_OFF & WINDIS_OFF )
_CONFIG2(IESO_OFF & FNOSC_FRCPLL & OSCIOFNC_OFF & POSCMOD_NONE &  FCKSM_CSECME & IOL1WAY_OFF)

/****************************************************************************/
/* Useful Macros */
#define BITS2WORD(sfrBitfield) ( *((unsigned int*) &sfrBitfield) )
// Convert a bitfield to a word (unsigned int).
#define BITS2BYTEL(sfrBitfield) ( ((unsigned char*) &sfrBitfield)[0] )
// Return the low byte (as a unsigned char) of a bitfield.
#define BITS2BYTEH(sfrBitfield) ( ((unsigned char*) &sfrBitfield)[1] )
// Return the high byte (as a unsigned char) of a bitfield.
        
        

//_CONFIG1(JTAGEN_OFF& FWDTEN_OFF);           //Turn off JTAG so we can use the pins
// (LPRC can be disabled by clearing SWDTEN bit in RCON register

void DelayTimer1Init(void) { // Set up timer 1 for microsecond delay function
    T1CON = 0b00011000; // Timer off, 16-bit, 1:1 prescale, gating off
}
/*
 1. If desired, read the COSCx bits
(OSCCON<14:12>) to determine the current
oscillator source.
2. Perform the unlock sequence to allow a write to
the OSCCON register high byte.
3. Write the appropriate value to the NOSCx bits
(OSCCON<10:8>) for the new oscillator source.
4. Perform the unlock sequence to allow a write to
the OSCCON register low byte.
5. Set the OSWEN bit to initiate the oscillator
switch.
 */
void OscInit(void) {
 
    OSCCONBITS OSCCONbitsCopy;

    // Copy the current Clock Setup
    OSCCONbitsCopy = OSCCONbits;
    
    
    // Check to see what clock setup is defined ? either internal or external
    //USE_FRC_CLOCK
    // Setup the uC to use the internal FRCPLL mode
    OSCCONbitsCopy.NOSC = 1;
    OSCCONbitsCopy.OSWEN = 1;
    //
    // Setup the uC to use the external crystal with the PLL
//    OSCCONbitsCopy.NOSC = 3;
//    OSCCONbitsCopy.OSWEN = 1;
    

    // Switch over to the new clock setup
    __builtin_write_OSCCONH( BITS2BYTEH( OSCCONbitsCopy ) );
    __builtin_write_OSCCONL( BITS2BYTEL( OSCCONbitsCopy ) );
    // Wait for this transfer to take place
    //while (OSCCONbits.COSC != OSCCONbits.NOSC);
    // Setup the DIV bits for the FRC, this values means the config word needs to be: PLLDIV_DIV2
    CLKDIVbits.RCDIV0 = 4;

    

    // At this point the PIC24FJ64GB004 clock setup will be complete with the PLL enabled 
}

void UsDelay(unsigned int udelay) {
    TMR1 = 0;
    PR1 = udelay * 15; // Number of ticks per microsecond
    IFS0bits.T1IF = 0; // Reset interrupt flag
    T1CONbits.TON = 1;
    while (!IFS0bits.T1IF); // Wait here for timeout
    T1CONbits.TON = 0;
}
void ADCInit(void)
{
    AD1CON1 = 0x00E4; // integer format, auto conversion,sample after conversion ends             

	AD1CON2 = 0x003C; // AVDD & AVSS as references,Disable Scan mode,interrupt after 16 sample/convert sequence,16 buffer levels, use MuxA              

    AD1CON3 = 0x0D09;// AD Clock as derivative of system clock,13TAD,16 samples are collected in 1mSec,Fcy =4Mhz 1Tcy=0.26uS              

	AD1CHS = 0x0005;// Positive sample input channel for MUX A to use AN5,Negative input channel for MUX A to use VR-

	AD1PCFG = 0xFFDF; //AN5 analog pin

 	AD1CSSL	= 0x0000; // Channel Scanning Disabled

}
int main (void)
{
    //Turn off SPI to allow extra digital outputs
    SPI1STATbits.SPIEN = 0;
    SPI1CON2bits.SPIBEN = 0;
 	
// Configure Oscillator to operate the device at 8Mhz
//    OscInit();

// Peripheral Initialisation
//   	initAdc1();             	// Initialize the A/D converter to convert Channel 0
//    initTmr3();					// Initialise the Timer to generate sampling event to ADC @ 8Khz rate
    ADCInit();
	InitUART2();	// Initialize UART2 for 9600,8,N,1 TX/RX


//    uint8_t hours, minutes;
    
            

    TRISBbits.TRISB6 = 0;   //set RB6 as an output
    TRISBbits.TRISB7 = 0;   //set RB7 as an output
    TRISBbits.TRISB8 = 0;   //set RB1 as an output
    TRISBbits.TRISB9 = 0;   //set RB0 as an output
    TRISBbits.TRISB10 = 0;   //set RB0 as an output
    TRISBbits.TRISB11 = 0;   //set RB3 as an output
    TRISBbits.TRISB12 = 0;   //set RB2 as an output
    TRISBbits.TRISB13 = 0;   //set RB2 as an output
    
    uint8_t a = 0;
    double T = 0;
    __C30_UART=2; //use UART2 for printf

    // Unlock Registers
    __builtin_write_OSCCONL(OSCCON & 0xBF);
    // Configure Input Functions (Table 10-2))
    // Assign U2RX
    RPINR19bits.U2RXR = U2RX_IO; //pin 14 green/org
    // Assign U2CTS 
    RPINR19bits.U2CTSR = U2CTS_IO;
    // Configure Output Functions (Table 10-3)
    // Assign U2TX (=FN #5)
    RPOR2bits.RP4R = 5; //pin 11 blue/yllw
    // Assign U2RT (=FN #6)
    RPOR3bits.RP6R = 6;
    
    // Lock Registers
    __builtin_write_OSCCONL(OSCCON | 0x40);

    ///////////////////////
    // Main Program Loop //
    ///////////////////////
//    DelayTimer1Init();
    AD1CON1bits.ADON = 1;     // Turn on the A/D converter
 while(1)
 {
//    uint16_t seconds;
//    for (seconds =0;seconds<65535; seconds++)
//    {}      
    while(!IFS0bits.AD1IF);  //waiting 
    IFS0bits.AD1IF = 0;	

    a = getADCAvg();
    T = a/ 1024.0 * 322; //vref in 10mV scale, for 10mV/C LM34
    UsDelay(10000); //wait 500ms
    //displayByte( a );
    //while(U2STAbits.UTXBF);
//    if (a != old_a ) {
        printf("Temperature: %f\n\r", T);
//        U2TXREG = a;
//        old_a = a;
//    }
    
            
            
//            displayByte(seconds);
            
    }
//}
return 0;

}
